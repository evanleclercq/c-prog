/*-------------------------------------------------------------------

 * File Name : inputTest.c
 * Created By : Evan le Clercq
 * Student ID : S3516635
 * Creation Date : 01-01-2016
 * Last Modified : Fri 01 Jan 2016 12:42:36 PM AEDT

 ------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main (void)
{

    char menuInput [2];

    printf ("\nWelcome to TicTacToe\n");
    printf ("--------------------\n");
    printf ("1. Play Game\n");
    printf ("2. Display High Scores\n");
    printf ("3. Quit\n\n");
    printf ("Please enter your choice: ");

    fgets (menuInput, 2, stdin);

    switch (menuInput[0]){
    case '1':
        printf ("Play Game Menu Selection Made\n\n");
        break;
    case '2':
        printf ("Display High Scores Menu Selection Made\n\n");
        break;
    case '3':
        printf ("Quit Menu Selection made\n\n");
        break;
    default:
        printf ("No Menu Selection Made");
        break;
    }

    EXIT_SUCCESS;

}
