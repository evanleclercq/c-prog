/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2015 Assignment #1
* Full Name        : Evan le Clercq
* Student Number   : S3516635
* Start up code provided by Paul Miller
***********************************************************************/
#include "scoreboard.h"

/**
 * @file scoreboard.c contains the implementation of functions to manage the
 * scoreboard
 **/

/**
 * @param scoreboard the scoreboard to initialise to an empty board
 **/
void init_scoreboard(score * scoreboard)
{
}

/**
 * @param scoreboard the scoreboard to add the score to
 * @param newscore the new score to add too the scoreboard
 * @return TRUE when a score is successfully added and FALSE otherwise
 **/
BOOLEAN add_to_scoreboard(score * scoreboard, const score* newscore)
{
        return FAILURE;
}

/**
 * @param scoreboard the scoreboard to display
 **/
void display_scores(score * scoreboard)
{
}
