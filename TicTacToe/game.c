/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2015 Assignment #1
* Full Name        : Evan le Clercq
* Student Number   : S3516635
* Start up code provided by Paul Miller
***********************************************************************/
#include "game.h"
#include "player.h"

/**
 * @file game.c contains the functions required for management of the game
 * while it is being played.
 **/

/**
 * @param human a pointer to the human player struct
 * @param computer a pointer to the computer player struct
 * @return a pointer to the winning player or NULL if there was no
 * winner
 **/
struct player * play_game(struct player * human, struct player * computer)
{
        return NULL;
}

/**
 * @param first the first player passed in
 * @param second the second player passed in
 **/
void swap_players(struct player** first, struct player** second)
{
}

/**
 * @param board the game_board passed in
 * @param cur_token
 **/
enum game_result test_for_winner(game_board board, enum cell cur_token)
{
        return GR_NOWINNER;
}


