/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2015 Assignment #1
* Full Name        : Evan le Clercq
* Student Number   : S3516635
* Start up code provided by Paul Miller
***********************************************************************/
#include "main.h"

/** @mainpage CPT220 - Assignment 1 documentation.
 *
 * This is the main html documentation of assignment 1 for CPT220 - Programming
 * in C for study period 4, 2015. You will find here a description of the
 * functions that you need to implement for your assignment.
 *
 * Please note that this should not be your first stop when starting your
 * assignment. Please read the assignment 1 specifications that are available
 * on blackboard before reading this documentation. The purpose of this
 * documentation is to clarify the actual individual requirements.
 *
 * This site presents you with the documentation for each function that you
 * need to implement broken down by the file that they exist in. Please go to
 * the <b>Files</b> tab and click on the file you wish to get more information
 * about and then click on that file. An explanation of each function
 * implemented in that file will be displayed.
 *
 **/

/**
 * @file main.c this file contains the main function call and any supporting
 * functions you may choose to implement such as for management of the main
 * menu.
 **/

/**
 * Entry point into the program - displays the main menu and controls the
 * overall flow of the program. Please interpret all comments without code in
 * this as hints at getting started with implementing this program - they are
 * the "larger dotpoints* for how the main menu is meant to work.
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void)
{
        /* variable that holds the menu choice selected by the user */
        enum menuchoice choice;
        /* scoreboard where the most recent games played is stored */
        score scoreboard[NUM_SCORES];
        /* structs representing the data for the human and computer players */
        struct player computer, human;
        /* a pointer to the winner - this will be NULL if the game was a draw 
         * or the human player chose to quit the game half way through
         */
        struct player *winner;
        /* initialise the scoreboard */
        /* get input from the user - which menu item would they like to
         * select?
         */
                /* process their menu choice */
                                /* handle a request to play a game */
                                        /* if there is a winner, display a
                                         * message about who the winner is and
                                         * add them to the scoreboard
                                         */
                                        /* otherwise, display a message that
                                         * there was no winner
                                         */
                                /* handle a request to display the high
				 * scores */
                                /* handle any invalid input */
                                /* ensure that you also handle a request to
                                 * quit the program*/

        gameMenu();

        return EXIT_SUCCESS;
}

void gameMenu ()
{

    char menuInput [2];

    printf ("\nWelcome to TicTacToe\n");
    printf ("--------------------\n");
    printf ("1. Play Game\n");
    printf ("2. Show High Scores\n");
    printf ("3. Exit Game\n\n");
    printf ("Please select an option: \n");

    fgets (menuInput, 2, stdin);

    switch (menuInput [0])
    {

        case '1':
            printf ("Game Starting");
            break;
        case '2':
            printd ("High Scores");
            break;
        case '3':
            printd ("Exiting Game");
            break;
        default:
            printf ("Invalid Option Selected. Please Try Again:\n");
            printf ("Please select an option: \n");
            break;

    }

}

