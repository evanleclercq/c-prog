/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2015 Assignment #1
* Full Name        : Evan le Clercq
* Student Number   : S3516635
* Start up code provided by Paul Miller
***********************************************************************/
#include "player.h"

/**
 * @file player.c contains functions to manage the @ref player data structure
 * which represents either a @ref HUMAN or @ref COMPUTER player
 **/

/**
 * @param human a pointer to the human player's struct @param token a pointer
 * to where a copy of the human player's token is stored so it can be accessed
 * later @return a result status indicating whether i/o was successful or not
 * or if the user pressed enter or ctrl-d on an empty line.
 **/
enum input_result init_human_player(struct player * human, enum cell * token)
{
        return FAILURE;
}

/**
 * @param computer a pointer to the computer player struct @param token the
 * token for the human player - the computer one needs to be the opposite
 * @return an indication of i/o success or failure which in this case can be
 * safely ignored as there is no i/o. It is only part of the interface so that
 * human and computer functions are called in a uniform way.
 **/
enum input_result init_computer_player(struct player * computer, enum cell token)
{
        return FAILURE;
}

/**
 * @param player the player to display the name of @return a pointer to the
 * player's name
 **/
const char * player_to_string(const struct player* player)
{
        return player->name;
}

/**
 * @param board the gameboard to insert the token into @param player a pointer
 * to the player struct that represents the current player.  @return a value
 * indicating whether any i/o operations were successful
 **/
enum input_result take_turn(game_board board, struct player * player)
{
}

