/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2015 Assignment #1
* Full Name        : Evan le Clercq
* Student Number   : S3516635
* Start up code provided by Paul Miller
***********************************************************************/
#include "board.h"

/**
 * @file board.c contains function implementations for managing the @ref
 * game_board
 **/

/**
 * @param board the game board to display
 **/
void init_board(game_board board)
{
}

/**
 * @param board the game board to display
 **/
void display_board(game_board board)
{
}
