/******************************************************************************
* CPT220 - Programming in C
* Study Period 4 2015 Assignment #1 
* Full Name        : Evan le Clercq
* Student Number   : S3516635
* Start up code provided by Paul Miller
******************************************************************************/

-------------------------------------------------------------------------------
CHANGE LOG
-------------------------------------------------------------------------------
        > Main Menu setup and input for selection working appropriately
        > Initialisation of gameboard working
        > Display of gameboard working - Empty, Cross and Nought displaying
        > Initialisation of both human and computer players
        > Checks for winner working - horizontal, vertical, diagonal
        > Main game loop implelemted (Work around used to swap players)
        > Initialisation of the scoreboard with empty structs
        > Printing out of the scoreboard added with correct space formatting
        > The take_turn function implelemted (different for player type)
        > Refactored all code to ensure tabs == 4 spaces
        > swap_players function working properly (commented out work around)
        > input validation for the coordinate input when making a move
        > Removed all compiler warning except read_rest_of_line (see below)

-------------------------------------------------------------------------------
DISPLAYING THE GAME BOARD
-------------------------------------------------------------------------------
The display_board() function is set up to take the first array index as the
horizontal coordinate and the second coordinate as the vertical coordinate.
It has been writted like this so it corresponds with the coordinate input
when the system asks where the player wants to place their token.


-------------------------------------------------------------------------------
CHOOSING PLAYER TOKENS
-------------------------------------------------------------------------------
The player tokens are chosen using the inbuilt C pseudorandom number 
generator rand(void). Once the random number has been generated the system
checks if it was an odd or even number (number % 2). If the number is even
(% 2 = 0) the human player is assigned to the Nought token, otherwise the
number is odd (% 2 = 1) and the player is assigned to the Cross token.


-------------------------------------------------------------------------------
SWAPPING PLAYERS
-------------------------------------------------------------------------------
While I could not get the swap_players function working properly I implemented
an alternative section of code which changed the pointers of the activePlayer
and otherPlayer variables within the player.c source. 

This has been left in the game.c source but is commented out as the swap_players
function is now working properly.


-------------------------------------------------------------------------------
WARNING REGARDING read_rest_of_line() FUNCTION
-------------------------------------------------------------------------------
A compiler warning is remaining in regards to the supplied funtion
read_rest_of_line in the helpers.c source code. I attempted to remove this
warning by declaring in a variety of different locations but in all cases I
ended up with the same result or having a warning stating it was declared but
not defined.

As a result I decided to not edit the supplied code in this case and accepted
that there would be one compiler warning present for the read_rest_of_line
function.


-------------------------------------------------------------------------------
PROBLEMS ENCOUNTERED WITH THE SCOREBOARD RELATED FUNCTIONS
-------------------------------------------------------------------------------
I encountered several issues with the scoreboard. 

The first occurred when it is initialised. There are several stucts in the 
array that contained faulty data somehow. I attempted to clear this out but it
did not work and as a result the scoreboard does not print out properly (empty)
when to program is first executed.

The second issue I encountered was when trying to add a winning player to the
scorebord array. Although using a test print line it seems to finish the
add_to_scoreboard function when the user attempts to view the scoreboard after
this it just exits the program. I have been unable to determine where this is
occuring and as a result it has not been fixed.

As a result of these issues the implementation of these requirements is
incomplete and the sorting function for the scoreboard has not been tested, 
however, it has been implelemted and it not generating and compiler warnings
or errors.


-------------------------------------------------------------------------------
INPUT VALIDATION
-------------------------------------------------------------------------------
Input validation caused a bit of an issue. The solution I have implelemted
caught most cases during testing. The one thing that I could not get working in
relation to this was the handling of the CRTL+D signal required. Every time I
tried a different method to do this it did not work properly and introduced
other issues into the program execution.


*******************************************************************************
gcc -ansi -Wall -pedantic -o run board.c main.c game.c helpers.c player.c scoreboard.c
*******************************************************************************
