/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2015 Assignment #1 
* Full Name        : Evan le Clercq
* Student Number   : S3516635
* Start up code provided by Paul Miller
***********************************************************************/
#include "game.h"
#include "player.h"

#define FULL_BOARD 9

struct player *activePlayer, *otherPlayer;
int exitGame = 1;

/**
 * @file game.c contains the functions required for management of the game
 * while it is being played.
 **/

/**
 * @param human a pointer to the human player struct
 * @param computer a pointer to the computer player struct
 * @return a pointer to the winning player or NULL if there was no
 * winner
 **/
struct player * play_game(struct player * human, struct player * computer)
{
    
    enum game_result result;
    struct player *winner = NULL;
    enum input_result inputResult;
  
  
    /* Determine who is the first player depending on token */
    if (human -> token == C_NOUGHT)
    {
        activePlayer = human;
        otherPlayer = computer;
    }
    else
    {
      activePlayer = computer;
      otherPlayer = human;
    }
  
  
    while (exitGame)
    {
      
        display_board(currentBoard);
        printf ("The current player is %s\n", player_to_string(activePlayer));     
      
        inputResult = take_turn (currentBoard, activePlayer);
      
        if (inputResult == FAILURE) {
            winner = NULL;
            break;
        }
      
        result = test_for_winner(currentBoard, activePlayer->token);
        
        if (result != GR_NOWINNER && result != GR_DRAW)
        {
            display_board(currentBoard);
            winner = activePlayer;
            /* return activePlayer; */
            break;
        }
        else if (result == GR_DRAW)
        {
            display_board(currentBoard);
            winner = NULL;
            /* return NULL; */
            break;
        }
      
        /* Simple swap player mecahnic
      
        if (activePlayer == human)
        {
            activePlayer = computer;
            otherPlayer = human;
        }
        else
        {
            activePlayer = human;
            otherPlayer = computer;
        } */

        swap_players(&activePlayer, &otherPlayer);
      
    }
  
    return winner;

}

/**
 * @param first the first player passed in
 * @param second the second player passed in
 **/
void swap_players(struct player** first, struct player** second)
{

    struct player *temp1 = *first;
    struct player *temp2 = *second;
  
    activePlayer = temp2;
    otherPlayer = temp1;
  
}

/**
 * @param board the game_board passed in
 * @param cur_token
 **/
enum game_result test_for_winner(game_board board, enum cell cur_token)
{
    int i, j, horCount, verCount, diagCount1, diagCount2, takenCells;
    enum game_result gameResult = GR_NOWINNER;

    verCount = 0;
    horCount = 0;
    diagCount1 = 0;
    diagCount2 = 0;  
   
    /* loops to check for matches along stright lines */
    for (i = 0; i < 3; ++i)
    {
    
        horCount = 0;
        verCount = 0;
    
        for (j = 0; j < 3; ++j)
        {
            /* Checks Vertical matches */
            if (board[i][j] == cur_token)
            {
                ++verCount;
            }

            /* Checks Horizonal matches */
            if (board[j][i] == cur_token)
            {
                ++horCount;
            }
        
        }
      
      if (verCount == 3 || horCount == 3) break;
    
    }

    /* check for matches along the horizonal posibilities */
    /* top left to bottom right check */
    diagCount1 = 0; 
  
    for (i = 0; i < 3; ++i)
    {      
        if (board[i][i] == cur_token)
        {
            ++diagCount1;
        }
    }
    /* top right to bottom left check */
    diagCount2 = 0;
    for (i = 2; i >= 0; --i)
    {
        if (i == 2) j = 0;
        else if (i == 1) j = 1;
        else if (i == 0) j = 2;
      
        if (board[i][j] == cur_token)
        {
            ++diagCount2;
        }
        
    }
  
    /* check if there are any empty cells left */
    takenCells = 0;
    for (i = 0; i < 3; ++i)
    {
        for (j = 0; j < 3; ++j)
        {
            if (board[i][j] != C_EMPTY)
            {
                ++takenCells;
            }
        }
    }
  
    
    /* Determine result of game */
    if (verCount == 3 || horCount == 3 || diagCount1 == 3 || diagCount2 == 3)
    {
        if (cur_token == C_NOUGHT)
        {
            /*printf ("NOUGHT WINS");*/
            printf ("%s is the winner!\n", player_to_string(activePlayer));
            gameResult = GR_NOUGHT;
            return GR_NOUGHT;
        }
        else if (cur_token == C_CROSS)
        {
            /*printf ("CROSS WINS");*/
            printf ("%s is the winner!\n", player_to_string(activePlayer));
            gameResult = GR_CROSS;
            return GR_CROSS;
        }
    }
    else if (takenCells == FULL_BOARD)
    {
        printf ("GAME DRAW");
        gameResult = GR_DRAW;
        return GR_DRAW;
    }
    else
    {
        gameResult = GR_NOWINNER;
        return GR_NOWINNER;
    }
  
  return gameResult;

}
