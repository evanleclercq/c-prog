/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2015 Assignment #1 
* Full Name        : Evan le Clercq
* Student Number   : S3516635
* Start up code provided by Paul Miller
***********************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "main.h"
#include "helpers.c"
#include <time.h>

/** @mainpage CPT220 - Assignment 1 documentation.
 *
 * This is the main html documentation of assignment 1 for CPT220 - Programming
 * in C for study period 4, 2015. You will find here a description of the
 * functions that you need to implement for your assignment.
 *
 * Please note that this should not be your first stop when starting your
 * assignment. Please read the assignment 1 specifications that are available
 * on blackboard before reading this documentation. The purpose of this
 * documentation is to clarify the actual individual requirements.
 *
 * This site presents you with the documentation for each function that you
 * need to implement broken down by the file that they exist in. Please go to
 * the <b>Files</b> tab and click on the file you wish to get more information
 * about and then click on that file. An explanation of each function
 * implemented in that file will be displayed.
 * 
 **/

/**
 * @file main.c this file contains the main function call and any supporting
 * functions you may choose to implement such as for management of the main
 * menu.
 **/

/**
 * Entry point into the program - displays the main menu and controls the
 * overall flow of the program. Please interpret all comments without code in
 * this as hints at getting started with implementing this program - they are
 * the "larger dotpoints* for how the main menu is meant to work.
 **/
int main(void)
{
    /* variable that holds the menu choice selected by the user */
    menuchoice choice;
    /* scoreboard where the most recent games played is stored */
    score scoreboard[NUM_SCORES];
    /* structs representing the data for the human and computer players */
    struct player computer, human;
    enum cell playerTok, compTok;
    /* a pointer to the winner - this will be NULL if the game was a draw 
     * or the human player chose to quit the game half way through
     */
    struct player *winner;
    /* initialise the scoreboard */
    /* get input from the user - which menu item would they like to
     * select?
     */
	
	  int cont = 1;
    int randGen;
    time_t t;
    enum input_result inputResult;

    init_board (currentBoard);
    init_scoreboard(scoreboard);
  
    /*TESTING SOME FUNCTIONS

    currentBoard[0][0] = C_NOUGHT;
    currentBoard[1][1] = C_NOUGHT;
    currentBoard[2][1] = C_NOUGHT;

    currentBoard[2][0] = C_NOUGHT;
    currentBoard[1][2] = C_NOUGHT;
    currentBoard[2][2] = C_CROSS;

    currentBoard[1][0] = C_CROSS;
    currentBoard[0][2] = C_CROSS;
    currentBoard[0][1] = C_CROSS;
  
    display_board (currentBoard);
  
    test_for_winner(currentBoard, C_NOUGHT);

    strcpy (scoreboard[0].name, "Evan");
    scoreboard[0].type = HUMAN;
    scoreboard[0].token = C_CROSS;

    BACK TO FUNCTIONALITY*/
  
  
    choice = showMenu();

    while (cont) {
	
        switch (choice) 
        {
            case play:
                printf ("Starting a new game.\n\n");
                /*start new game*/

                /*Randomise player tokens*/
                srand((unsigned) time(&t));
                randGen = rand();
         
                if (randGen % 2 == 1)
                {
                    playerTok = C_CROSS;
                    compTok = C_NOUGHT;
                }
                else
                {
                    playerTok = C_NOUGHT;
                    compTok = C_CROSS;
                }
         
                /*Initialise players*/
                inputResult = init_human_player (&human, &playerTok);
            
                if (inputResult == SUCCESS)
                {
                    init_computer_player (&computer, compTok);

                    /*Initialise game board*/
                    init_board(currentBoard);

                    winner = play_game (&human, &computer);

                    if (winner != NULL)
                    {
                        add_to_scoreboard (&scoreboard[NUM_SCORES], winner);
                    } 
                }
                
                /* show main menu again after game completed*/
                choice = showMenu();
                break;
            
            case highscore:
                printf ("Displaying High Scores Board\n\n");
                /*show score board*/
                display_scores(&scoreboard[NUM_SCORES]);
                choice = showMenu();
                break;
            
            case gameexit:
                printf ("Exiting the game.\n\n");
                cont = 0;
                break;
            
            case invalid:
                printf ("Please try again.\n\n");
                choice = showMenu();
                break;
        }

	 }
	
    /* process their menu choice */
        /* handle a request to play a game */
            /* if there is a winner, display a
             * message about who the winner is and
             * add them to the scoreboard
             */
            /* otherwise, display a message that
             * there was no winner
             */
        /* handle a request to display the high
         * scores */

	      /* handle any invalid input */
        /* ensure that you also handle a request to
         * quit the program
         */

    return EXIT_SUCCESS;
  
}

menuchoice showMenu ()
{
    char menuInput [2];

    printf ("\nWelcome to TicTacToe\n");
    printf ("--------------------\n");
    printf ("1. Play Game\n");
    printf ("2. Display High Scores\n");
	  printf ("3. Quit\n\n");
	  printf ("Please enter your choice: ");
	
	  fgets(menuInput, 2, stdin);
  
    /*makes sure that the buffer is empty after unput*/
    if (!feof(stdin)) read_rest_of_line();

    if (menuInput[0] == '1') 
    {
        return play;
    }
    else if (menuInput[0] == '2')
    {
        return highscore;
    }
    else if (menuInput[0] == '3' || strcmp(menuInput, "^D"))
    {
        return gameexit;
    }
    else
    {
        printf ("Invalid option selected\n\n");
        return invalid;
    }

}
