/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2015 Assignment #1 
* Full Name        : Evan le Clercq
* Student Number   : S3516635
* Start up code provided by Paul Miller
***********************************************************************/
#include "board.h"

game_board currentBoard;

/**
 * @file board.c contains function implementations for managing the @ref
 * game_board
 **/

/**
 * @param board the game board to display
 **/
void init_board(game_board board)
{
    int i, j;
    /*for loop to go through board height*/
    for (i = 0; i < BOARDHEIGHT; ++i) 
    {
        /*for loop to go through board width*/
        for (j = 0; j < BOARDWIDTH; ++j) 
        {    
        /*Assigning each cell with the empty cell enum*/
        board[i][j] = C_EMPTY;
        }    
    }
  
}

/**
 * @param board the game board to display
 **/
void display_board(game_board board)
{
  
    int i = 0;
  
    /* Showing the top coordinates */   
    printf ("\n|---------------|\n");
    printf ("|   | A | B | C |\n");
    printf ("|---------------|\n");
  
    /* Print out first row of cells*/
    printf ("| 1 |");
    for (i = 0; i < BOARDWIDTH; ++i)
    {
    
        switch (board[i][0]) 
        {  
            case C_EMPTY:
                printf ("   |");
                break;
            
            case C_CROSS:
                printf (" X |");
                break;
            
            case C_NOUGHT:
                printf (" O |");
                break;
      
            case C_INVALID:
                printf ("ERR|");
                break;
        }
    
    }

    printf ("\n|---------------|\n");
  
    /* Print out second row of cells*/
    printf ("| 2 |");
    
    for (i = 0; i < BOARDWIDTH; ++i)
    {
    
        switch (board[i][1]) 
        {
            case C_EMPTY:
                printf ("   |");
                break;
            
            case C_CROSS:
                printf (" X |");
                break;
            
            case C_NOUGHT:
                printf (" O |");
                break;
            
            case C_INVALID:
                printf ("ERR|");
                break;
        }
    
    }
  
    printf ("\n|---------------|\n");
 
    /* Print out third row of cells*/
    printf ("| 3 |");
  
    for (i = 0; i < BOARDWIDTH; ++i)
    {
    
        switch (board[i][2]) 
        {  
            case C_EMPTY:
                printf ("   |");
                break;
            
            case C_CROSS:
                printf (" X |");
                break;
            
            case C_NOUGHT:
                printf (" O |");
                break;
            
            case C_INVALID:
                printf ("ERR|");
                break;
        }
    
    }
  
    printf ("\n|---------------|\n");
  
}

