/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2015 Assignment #1 
* Full Name        : Evan le Clercq
* Student Number   : S3516635
* Start up code provided by Paul Miller
***********************************************************************/
#include "player.h"
#include "helpers.c"

/**
 * @file player.c contains functions to manage the @ref player data structure
 * which represents either a @ref HUMAN or @ref COMPUTER player
 **/

int usedToken;

/**
 * @param human a pointer to the human player's struct @param token a pointer
 * to where a copy of the human player's token is stored so it can be accessed
 * later @return a result status indicating whether i/o was successful or not
 * or if the user pressed enter or ctrl-d on an empty line.
 **/
enum input_result init_human_player(struct player * human, enum cell * token)
{

    int randGen;
    char str [NAMELEN + 1];
    enum input_result inputResult = FAILURE;

    human->type = HUMAN;
    human->token = *token;
  
    /*Ask for the player name input*/  
    printf("Please enter the name of the player: ");
    
    fgets (str, NAMELEN + 1, stdin);
  
    /*makes sure that the buffer is empty after unput*/
    /*if (!feof(stdin)) read_rest_of_line();*/
  
    /*input validation for the player name*/
  
    if (str[1] == '\0')
    {
        inputResult = FAILURE;
        printf ("No name entered.\nReturning to main menu.\n\n");
    }
    else
    {
        strcpy (human->name, str);
        inputResult = SUCCESS;
        randGen = rand();
    }

    return inputResult;
}

/**
 * @param computer a pointer to the computer player struct @param token the
 * token for the human player - the computer one needs to be the opposite
 * @return an indication of i/o success or failure which in this case can be
 * safely ignored as there is no i/o. It is only part of the interface so that
 * human and computer functions are called in a uniform way.
 **/
enum input_result init_computer_player(struct player * computer, enum cell token)
{ 
    strcpy(computer->name, "COMPUTER");
    computer->token = token;
    computer->type = COMPUTER;
  
    return FAILURE;
}

/**
 * @param player the player to display the name of @return a pointer to the
 * player's name
 **/
const char * player_to_string(const struct player* player)
{
    return player->name;
}

/**
 * @param board the gameboard to insert the token into @param player a pointer
 * to the player struct that represents the current player.  @return a value
 * indicating whether any i/o operations were successful
 **/
enum input_result take_turn(game_board board, struct player * player)
{
  
    enum input_result moveMade = FAILURE;
    enum player_type currentPlayer = player->type;
    int verCoord, horCoord, i;
    char moveAttempt [10];
    char *token;
    char inputChar, inputInt;
    int regex = 0;
  
    switch (currentPlayer)
    {
        case HUMAN:
            while (moveMade == FAILURE)
            {
                printf ("Please enter a comma separated coordinate such as 'A,1'");
                printf (" for where you wish to place your token: ");
                fgets (moveAttempt, 10, stdin);

                /* checks if any input entered */
                if (moveAttempt[1] == '\0') {
                    return FAILURE;
                }
              
                for (i = 0; i < strlen(moveAttempt); ++i)
                {
                    if (moveAttempt[i] == ',')
                    {
                      regex = 1;
                    }
                }
              
                /* printf ("Regex = %d\n", regex); */

                /* Check if the input is too long or too short */
                if (strlen (moveAttempt) > 4 || strlen(moveAttempt) < 3)
                {
                    printf ("Invalid input. Please try again.\n");
                }
                else if (regex == 0)
                {
                    printf ("Invalid input. Please try again.\n");
                }
                else
                {
                    token = strtok (moveAttempt, ",");
                  
                    if (sizeof(*token) == sizeof(inputChar))
                    {
                        inputChar = *token;
                    }
                    token = strtok (NULL, ",");
                    if (sizeof(*token) == sizeof(inputInt))
                    {
                    inputInt = *token;
                    }
                      
                    if ((inputChar != 'A') && (inputChar != 'B') && (inputChar != 'C'))
                    {
                        /* printf ("TEST CHAR\n"); */
                        printf ("Invalid input. Please try again.\n");
                    }
                    else if (inputInt != '1' && inputInt != '2' && inputInt != '3')
                    {
                        /* printf ("TEST INT\n"); */
                        printf ("Invalid input. Please try again.\n");
                    }
                    else
                    {
                        if (inputChar == 'A') horCoord = 0;
                        if (inputChar == 'B') horCoord = 1;
                        if (inputChar == 'C') horCoord = 2;
                      
                        if (inputInt == '1') verCoord = 0;
                        if (inputInt == '2') verCoord = 1;
                        if (inputInt == '3') verCoord = 2;

                        /* verCoord = (strtol (*token, &eptr, 10))-1; */

                        /*if (!feof(stdin)) read_rest_of_line();*/

                        if (board[horCoord][verCoord] == C_EMPTY)
                        {
                            board[horCoord][verCoord] = player->token;
                            display_board(board);
                            moveMade = SUCCESS;
                            break;
                            
                        }
                    }
                }
            }
        
            return moveMade;
        
        case COMPUTER:
            while (moveMade == FAILURE)
            {

                verCoord = rand() % 3;
                horCoord = rand() % 3;
              
                /*printf ("%d\n", rand());*/

                if (horCoord == 0) printf ("The computer tries to move at A,%d ...\n", verCoord + 1);
                else if (horCoord == 1) printf ("The computer tries to move at B,%d ...\n", verCoord + 1);
                else if (horCoord == 2) printf ("The computer tries to move at C,%d ...\n", verCoord + 1);

                if (board[horCoord][verCoord] == C_EMPTY)
                {
                    board[horCoord][verCoord] = player->token;
                    printf ("And it was successful!\n");
                    moveMade = SUCCESS;
                }
              
            }
        
            break;
        
    }
  
    return moveMade;
  
}

