/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2015 Assignment #1 
* Full Name        : Evan le Clercq
* Student Number   : S3516635
* Start up code provided by Paul Miller
***********************************************************************/
#include "scoreboard.h"

/**
 * @file scoreboard.c contains the implementation of functions to manage the
 * scoreboard
 **/

/**
 * @param scoreboard the scoreboard to initialise to an empty board
 **/
void init_scoreboard(score * scoreboard)
{
    int i, j;
    struct player temp;
  
    strcpy (temp.name, "");

    /* scoreboard = malloc (NUM_SCORES * sizeof(score)); */
  
    /* Initialise each index of the scoreboard array with an empty
     * player struct.
     */
    for (i = 0; i < NUM_SCORES; ++i)
    {
        scoreboard[i] = temp;
      
        for (j = 0; j < strlen(scoreboard[i].name); ++i) {
          scoreboard[i].name[j] = 0;
        }
        /* attempting to ensure the name variable was empty */
        strcpy (scoreboard[i].name, "");
    }
  
}

/**
 * @param scoreboard the scoreboard to add the score to
 * @param newscore the new score to add too the scoreboard
 * @return TRUE when a score is successfully added and FALSE otherwise
 **/
BOOLEAN add_to_scoreboard(score * scoreboard, const score* newscore)
{
    int i;
    BOOLEAN bool = FALSE;
  
    for (i = 0; i < NUM_SCORES; ++i)
    {
      
        if (strcmp (scoreboard[i].name, "") == 0)
        {
          
            strcpy (scoreboard[i].name, newscore->name);
            scoreboard[i].type = newscore->type;
            scoreboard[i].token = newscore->token;
            bool = TRUE;
          
            /*printf ("ADDED"); */
            break;
          
        }
      
    }
  
    return bool;
  
}

/**
 * @param scoreboard the scoreboard to display
 **/
void display_scores(score * scoreboard)
{
    int i;
  
    printf ("\n-----------------------------------------------\n");
    printf ("TicTacToe Winners\n");
    printf ("-----------------------------------------------\n");
    printf ("Name                |Player Type    |Token Type \n");
    printf ("-----------------------------------------------\n");

    for (i = 0; i < NUM_SCORES; ++i)
    {
        char playerName[20];
        char playerType[8];
        char playerToken[6];
      
        sort_scoreboard (scoreboard);
      
        if (strcmp (scoreboard[i].name, "") != 0) {
      
            strcpy (playerName, scoreboard[i].name);

            /* Determine string for type field */
            if (scoreboard[i].type == HUMAN)
            {
                strcpy (playerType, "Human"); 
            }
            else if (scoreboard[i].type == COMPUTER)
            {
                strcpy (playerType, "Computer");
            }
            else 
            {
                strcpy (playerType, "");
            }

            /* Determine string for token field */
            if (scoreboard[i].token == C_CROSS)
            {
                strcpy (playerToken, "Cross");
            }
            else if (scoreboard[i].token == C_NOUGHT)
            {
                strcpy (playerToken, "Nought");
            }
            else 
            {
                strcpy (playerToken, "");
            }

            /* print out the strings with appropriate spacing */
            printf ("%-20s|%-15s|%-10s\n", playerName, playerType, playerToken);
          
        }
      
    }
    
}

/* Function to sort the scoreboard.*/
void sort_scoreboard (score * scoreboard)
{

  int swapped, i;
  
  while (1)
  {
    swapped = 0;
    
    for (i = 0; i < NUM_SCORES - 1; ++i)
    {
      if (scoreboard[i].name[0] > scoreboard[i+1].name[0])
      {
        struct player temp = scoreboard[i];
        scoreboard[i] = scoreboard[i+1];
        scoreboard[i+1] = temp;
        swapped = 1;
      }
    }
    
    if (swapped == 0)
    {
        break;
    }
  }
  
  
  
}
