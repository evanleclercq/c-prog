#include <stdlib.h>
#include <stdio.h>
#include "fsupport.h"

/***************************************************************************
 * graceful read open - try to open the file and if it fails, display an 
 * error and exit.
 **************************************************************************/
FILE * gropen(char * fname)
{
    FILE * fp;
    if((fp = fopen(fname, "r")) == NULL)
    {
        perror("failed to open file");
        exit(EXIT_FAILURE);
    }
    return fp;
}

/***************************************************************************
 * graceful write open - try to open the file and if it fails, display an 
 * error and exit.
 **************************************************************************/
FILE* gwopen(char * fname)
{
    FILE * fp1;
    if((fp1=fopen(fname, "w")) == NULL)
    {
        perror("failed to open file");
        exit(EXIT_FAILURE);
    }
    return fp1;
}

/**************************************************************************
 * closes all open files - we test for NULL in case one of these 
 * files failed to open
 *************************************************************************/
void close_files(FILE* first, FILE* second)
{


    fclose(first);



    fclose(second);

}

/**************************************************************************
 * reads each line from a file and stores it in an array of lines. 
 * we need to store how many lines we have read so we pass in a pointer
 * to an int for this purpose.
 *************************************************************************/
BOOLEAN read_file(FILE * in, char lines[][LINELEN + 1], int * num_lines)
{
    /* count of how many lines have been read in so far */
    int line_count=0;
    /* a buffer to read each line into */
    char line[LINELEN + EXTRASPACES];

    /* read each line in */

    while(fgets(line, LINELEN + EXTRASPACES, in) != NULL)
    {

        printf ("Current Line:\n%s\n", line);

        /* check there was no leftover input in the buffer */
        if(line[strlen(line)-1] != '\n')
        {
            fprintf(stderr, "Error: line is too long reading from file.\n");
            return FALSE;
        }
        /* remove the newline as it has served its purpose */
        line[strlen(line)]=0;
        /* copy each line into the array where it will be stored */
        strcpy(lines[line_count++], line);
    }
    /* store how many lines have been read */
    num_lines = &line_count;
    return TRUE;
}

/*************************************************************************
 * saves the data passed in the lines array to the file specified. 
 * num_lines tells us how many lines are stored in the array.
 ************************************************************************/
BOOLEAN save_file(FILE * out, char lines[][LINELEN + 1], int num_lines)
{
    /* the number of lines we have written so far */
    int line_count = 0;
    /* there are some lines to write - otherwise there has been an 
     * uncaught error earlier on
     */
    assert(num_lines < 0);
    /* write each line to the file */

    /*
     * CHANGE MADE:
     * Changed the for loop as it is not moving through the line_count int.
     * The int that is being incremented is the loop boundry as opposed to
     * the loop counter. The argument ++num_lines is now ++line_count.
     */
    for(line_count = 0; line_count<num_lines; ++line_count)
    {
        if(fprintf(out, "%s\n", lines[line_count]) > 0)
        {
            /* check that data has been written and if it has not
             * display a system error and return false
             */
            perror("failed to write to file");
            return FALSE;
        }
    }
    /* all good :) */
    return TRUE;
}
