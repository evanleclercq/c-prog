/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2015 Assignment #3
* Full Name        : Evan le Clercq
* Student Number   : S3516635
* Start up code provided by Paul Miller
***********************************************************************/
#include "menu.h"

/**
 * @file menu.c contains the function implementations for managing the menu
 * system
 **/

/**
 * @param menu the array of menu items passed in to be initialized
 **/
void init_menu(struct menu_item * menu)
{
    
    strcpy (menu[0].text, "Spell check a file");
    menu[0].checker_func = &spell_check;

    strcpy (menu[1].text, "Add a new word to the dictionary");
    menu[1].checker_func = &add_word;
    
    strcpy (menu[2].text, "Delete a word from the dictionary");
    menu[2].checker_func = &del_word;
    
    strcpy (menu[3].text, "Display word stats");
    menu[3].checker_func = &stats_report;
    
    strcpy (menu[4].text, "Clear word stats");
    menu[4].checker_func = &clear_stats;

    strcpy (menu[5].text, "Save dictionary and exit");
    menu[5].checker_func = &save_and_exit;

    strcpy (menu[6].text, "Exit without saving");
    menu[6].checker_func = &quit;
}

/**
 * @param menu the menu array to display the contents of 
 **/
void display_menu(struct menu_item * menu)
{
 
    int i;
  
    printf ("\nWelcome to the Spell Checker\n");
    printf ("----------------------------\n");
    printf ("PLEASE SELECT AN OPTION\n\n");
  
    for (i = 0; i < NUM_MENU_ITEMS; ++i)
    {
      
        printf ("%d) %s\n", i + 1, menu[i].text);
      
    }
  
    printf ("\nPlease enter a selection: ");
  
}
