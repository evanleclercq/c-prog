###############################################################################
# CPT220 - Programming in C
# Study Period 4 2015 Assignment #3
# Full Name        : Evan le Clercq
# Student Number   : S3516635
# Start up code provided by Paul Miller
###############################################################################

===============================================================================
IMPLEMENTATIONS COMPLETED
===============================================================================
        - Makefile completed and tested.
        - Validation for number of command line arguments.
        - Dictionary files being read in properly.
        - Menu structs initialised and displaying.
        - Input for menu selection working.
        - Option 2) Add new word to dictionary working for all cases.
        - Option 3) Remove word from dictionary working for all cases.
        - Option 6) Save and exit writing to file. Writing to file working.
        - Option 4) Display word stats working for all cases.
        - Option 5) Clear word stats working properly.
        - Option 1) Spell check a file working properly.
        - Option 7) Exit without saving partially working (see below).
        - Fixed issue with menu input being invalid.
        - Created input check function to look for Ctrl+D or new line.
        - Implemented 'back to menu' for Ctrl+D and new line cases.
        - Implemented system exit for Ctrl+D and new line cases at menu select.
        - Went through code ensuring all lines are < 80 characters.
        
        
===============================================================================
MAKE FILE
===============================================================================
Make file edited to be reusable.
All defining of file names are contained with variables.


===============================================================================
FREE MEMORY PROBLEMS
===============================================================================
Encountered problems when freeing memory during the exit functions. Encountered
error: *** glibc detected ***

In the end the list_free function woks properly and the memory used to store
the list information is returned to the system. Still encountering the same
problem when trying to free the checker system. I think this is becuase it is
trying to free the list variable again even though it has already been
successfully freed. Given this and the fact that trying to free the file_name
variable was giving a compliation warning, I did not end up freeing the 
checker system variable manually.

===============================================================================

