/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2015 Assignment #3
* Full Name        : Evan le Clercq
* Student Number   : S3516635
* Start up code provided by Paul Miller
***********************************************************************/
#include "checker.h"
#include "list.h"
#include "menu.h"

/* Defining constants */
#define NUMARGS 2 /* Define number of expected arguments */
#define FILEARG 1 /* location of file in argv index */
#define EXIT 7
#define SAVE_EXIT 6

/**
 * @mainpage This is the main html documentation of assignment 3 for
 * CPT220 - Programming in C for study period 4, 2015. You will find
 * here a description of the functions that you need to implement for
 * your assignment.
 *
 * Please note that this should not be your first stop when starting
 * your assignment. Please read the assignment 1 specifications that
 * are available on blackboard before reading this documentation. The
 * purpose of this documentation is to clarify the actual individual
 * requirements.
 *
 * This site presents you with the documentation for each function
 * that you need to implement broken down by the file that they exist
 * in. Please go to the Files tab and click on the file you wish to
 * get more information about and then click on that file. An
 * explanation of each function implemented in that file will be
 * displayed.
 **/

/**
 * @file checker.c
 * contains the main function and any related helper files
 **/

/**
 * the entry point for your program. From here you should initialise 
 * datastructures and load data into the system and then manage the menu
 * loop. Once the user has decided to quit, you should make sure any resources
 * required by the system are released and cleaned up. 
 **/
int main(int argc, char** argv)
{
    
    /* array of menu items including the text for each menu item and a 
     * pointer to the callback function
     */
    struct menu_item menu[NUM_MENU_ITEMS];
    BOOLEAN quit = FALSE;
    BOOLEAN result, inputFail = FALSE;
    /* holds all the data for our system */
    struct checker_system checker;
  
    /*FILE * fp;*/
    int menuChoice;
    char * input = (char *) malloc (MAX_INPUT);
    char * end;

      
    /* Check for the correct number of command line arguments */
    if (argc != NUMARGS)
    {
        fprintf (stderr, "\nError: invalid arguments entered.\n");
        printf ("The correct way to run this program is:\n");
        printf ("        ./checker dictfile\n");
        printf ("Where dictfile is a line separated file of words\n\n");
      
        return EXIT_FAILURE;
    }
    
    /* Check if the command line argurment is a line separated file */
  
    
	
    /* ensure that the data structures are initialized to a safe, 
     * empty state
     */
    
    system_init(&checker);

    /* load the data into the system */
    load_data(&checker, argv[FILEARG]);

    /* print out the size of our dictionary */
    
    /* initialize the menu data structure */
    
    init_menu (menu);
  
    while (!quit)
    {
 
        display_menu (menu);

        /* get choice from the user */
        /* get input and convert to integer*/  
        do
        {
            (get_input(input, TRUE));

            if (!check_input(input))
            {
                input = "7";
                inputFail = TRUE;
            }
            else if (strlen (input) >= 2 || isdigit(input[0]) == 0)
            {
                printf ("Error: Input not valid. Please try again.\n\n");
                printf ("Please enter a selection: ");
            }
            else
            {
                inputFail = TRUE;
            }
          
        } while (!inputFail);
  
        menuChoice = strtol (input, &end, 10);

        /* run the requested checker function */
        if (menuChoice == SAVE_EXIT || menuChoice == EXIT)
        {
            quit = menu[menuChoice-1].checker_func(&checker);
            result = quit;
        }
        else
        {
            result = menu[menuChoice-1].checker_func(&checker);
        }

        if (!result)
        {
            printf ("'%s' failed to run successfully.\n",
                    menu[menuChoice-1].text);
        }
      
    }
                        
    return EXIT_SUCCESS;
}
