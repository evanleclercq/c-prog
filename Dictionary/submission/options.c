/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2015 Assignment #3
* Full Name        : Evan le Clercq
* Student Number   : S3516635
* Start up code provided by Paul Miller
***********************************************************************/
#include "options.h"

/**
 * @file options.c contains the implementations of all functions for implementing
 * the options as specificied in the assignment specification
 **/

/**
 * @param checker datastructure that holds all data for managing the system
 * @return @ref TRUE when we successfully spellcheck a file and @ref FALSE
 * otherwise
 **/
BOOLEAN spell_check(struct checker_system* checker)
{
    char * filepath = (char*)malloc (LINE_LENGTH);
    char * input = (char*)malloc (LINE_LENGTH);
    FILE * fp;
    char * token = (char*)malloc (MAX_INPUT);
    struct word * newWord;
    struct word_list check_list;
    int i = 0;
    struct list_node * dict_current;
    struct list_node * check_current;


    list_init (&check_list);

    printf ("Please enter the path of the file you want to check.\n");
    printf ("It must be a plain text file: ");

    get_input (filepath, FALSE);

    /* check for ctrl+d or new line input */
    if (!check_input (input))
    {
        printf ("Error: no valid input entered");
        return FALSE;
    }

    if (!(fp = fopen (filepath, "r")))
    {
        printf("Failed to open file for spell checking."
               " No such file or directory\n");
        return FALSE;
    }

        while (fgets (input, LINE_LENGTH+1, fp) != NULL)
    {
        token = strtok (input, DELIMIT);

        if (strlen (input) > 1)
        {
            do
            {
                i = 0;
                while (token[i])
                {
                    token[i] = tolower(token[i]);
                    ++i;
                }

                newWord = malloc (sizeof (struct word));
                newWord = make_word (token);
                list_add (&check_list, newWord);

                dict_current = checker->list->head;
                while (dict_current != NULL)
                {
                    if (strcmp (newWord->word, dict_current->data->word) == 0)
                    {
                        newWord->count = 1;
                        ++dict_current->data->count;
                    }

                    dict_current = dict_current->next;
                }

                list_add (&check_list, newWord);
              
            } while ((token = strtok(NULL, DELIMIT)) != NULL);
        }
    }
    
    fclose (fp);
    
    printf ("The following words were in your text but missing from"
            " our dictionary:\n");
    
    check_current = check_list.head;
    while (check_current != NULL)
    {
        if ((check_current->data->count) == 0)
        {
          printf ("%s, ", check_current->data->word);
        }

        check_current = check_current->next;
    }
  
    printf ("\n");
  
    /* Freeing malloced variables */
    free(filepath);
    free(input);
    free(token);
  
    return TRUE;
}

/**
 * @param checker datastructure that holds all the data for the system
 **/
BOOLEAN add_word(struct checker_system* checker)
{
  
    char * input = (char *) malloc (MAX_INPUT);
    struct word * newword;
    int i = 0;
    BOOLEAN result = FALSE;
  
    printf ("\nPlease enter a word to add to our dictionary." 
            " Note it will be converted to lower case and will not be" 
            " added if it already exists: ");
  
    /* get input from user */
    get_input(input, TRUE);
  
    /* check for ctrl+d or new line input */
    if (!check_input (input))
    {
        printf ("Error: no valid input entered");
        return FALSE;
    }
  
    /* make sure input does not contain integers */
    i = 0;
    while (input[i])
    {
        if (isdigit(input[i]) != 0)
        {
            printf ("Error: '%s' is not a valid word\n", input);
            return FALSE;
        }
        i++;
    }

    newword = malloc (sizeof (struct word));
    newword = make_word (input);
  
    result = list_add (checker->list, newword);
  
    if (!result) 
    {
        return FALSE;
    }
    else
    {
        printf ("The word '%s' has been successfully"
                " added to the dictionary\n", input);
        return TRUE;
    }
  
  
    free (input);
    return result;
  
}

/**
 * @param checker datastructure that holds all the data for the system
 **/
BOOLEAN del_word(struct checker_system* checker)
{
   
    char * input = (char *) malloc (MAX_INPUT);
    BOOLEAN result = FALSE;
    int i = 0;
    struct word * word;
    
    printf ("\nPlease enter a word to delete from our dictionary: ");
    get_input(input, TRUE);
  
    /* check for ctrl+d or new line input */
    if (!check_input (input))
    {
        printf ("Error: no valid input entered");
        return FALSE;
    }
  
    /* make sure input does not contain integers */
    i = 0;
    while (input[i])
    {
        if (isdigit(input[i]) != 0)
        {
            printf ("Error: %s is not a valid word\n", input);
            return FALSE;
        }
        i++;
    }
  
    word = malloc (sizeof (struct word));
    word = make_word (input);
  
    result = list_del (checker->list, word);
  
    if (!result)
    {
        printf ("Error: The item was not found.\n");
        printf ("Error: '%s' was not found in dictionary.\n", word->word);
        return FALSE;
    }
    else
    {
        printf ("'%s' was successfully removed from the dictionary", input);
        return TRUE;
    }
  
   return result;

}

/**
 * @param checker datastructure that maintains the data for the system
 **/
BOOLEAN stats_report(struct checker_system * checker)
{
    
    struct list_node * current, * prev = NULL;
    BOOLEAN found = FALSE;
    char *input = (char *) malloc (MAX_INPUT);
    char inChar = '\0';
    int i = 0;
  
    printf ("\nThis program allows you to output basic stats about all words" 
            " that start with the same letter.\n");
    printf ("What letter would you like to report on: ");
      
    get_input(input, TRUE);
  
    /* check for ctrl+d or new line input */
    if (!check_input (input))
    {
        printf ("Error: no valid input entered");
        return FALSE;
    }
  
    /* make sure input does not contain integers */
    i = 0;
    while (input[i])
    {
        if (isdigit(input[i]) != 0)
        {
            printf ("Error: %s is not a valid word\n", input);
            return FALSE;
        }
        i++;
    }
  
    if (strlen(input) != 1)
    {
        printf ("Error: Line entered was invalid. Please try again.\n\n");
        stats_report(checker);
    }
  
    inChar = (char) input[0];
  
    current = checker->list->head;
  
    while (current != NULL)
    {
        if (current->data->word[0] == inChar)
        {
            if (current->data->count > 0)
            {
                found = TRUE;
                break;
            }
        }
        
        prev = current;
        current = current->next;
    }
  
    current = checker->list->head;
    prev = NULL;
  
    if (found)
    {
      
        printf ("\nWord                                         "
                "| Word Frequency\n");
        printf ("--------------------------------------"
                "-----------------------\n");
      
        current = checker->list->head;
  
        while (current != NULL)
        {
            if (current->data->word[0] == inChar)
            {
                if (current->data->count > 0)
                {
                    printf ("%-44s | %-14d\n",
                            current->data->word, current->data->count);
                }
            }

            prev = current;
            current = prev->next;
        }
  

  
         printf ("------------------------------------------"
                 "-------------------\n");
      
    }
    else
    {
        printf ("\nError: There were no words that start" 
                "with that letter '%s'\n", input);
        return FALSE;
    }
  
    free(input);
  
  	return TRUE;

}

/**
 * @param checker the datastructures that help us manage the overall system
 **/
BOOLEAN clear_stats (struct checker_system * checker)
{

    struct list_node * current;

    current = checker->list->head;

    while (current != NULL)
    {
        current->data->count = 0;
        current = current->next;
    }

    printf ("\nSuccessfully reset the stats on all words to 0\n");

    return TRUE;

}

/**
 * @param checker the datastructures that help us manage the system
 **/
BOOLEAN save_and_exit(struct checker_system* checker)
{
    struct list_node * current;
    FILE * fp;

    fp = fopen (checker->word_file, "w+");

    current = checker->list->head;

    while (current != NULL)
    {
        /*printf ("%s, ", current->data->word);*/

        fputs (current->data->word, fp);
        fputs ("\n", fp);
 
        current = current->next;
    }

    fclose (fp);

    printf ("\nExiting the program.\n");
    system_free (checker);

    return TRUE;
}

/**
 * @param checker the datastructures that help us manage the system
 **/
BOOLEAN quit(struct checker_system* checker)
{
    system_free (checker);
    printf ("\nExiting the program.\n");
    return TRUE;
}
