/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2015 Assignment #3
* Full Name        : Evan le Clercq
* Student Number   : S3516635
* Start up code provided by Paul Miller
***********************************************************************/
#include "utility.h"
#include "list.h"
/**
 * @file utility.c contains implementations of helper functions 
 * and i/o functions for the program
 **/

/**
 * clear the input buffer when the user has entered more data than allowed.
 * You should only call this when you have detected leftover input in the 
 * input buffer
 **/
static void read_rest_of_line(void)
{
  	int ch;
	  while(ch = getc(stdin), ch != '\n' && ch != EOF)
	  	;
	  clearerr(stdin);
}

/**
 * @param checker the datastructure that holds all the data for the
 * system
 **/
BOOLEAN system_init(struct checker_system * checker)
{
    /*memset (checker, 0, sizeof(struct checker_system));*/
    checker->list = malloc (sizeof (struct word_list));
    list_init (checker->list);
	  return TRUE;
}

/**
 * @param checker the datastructure that maintains all the information 
 * about the system
 * @param filename the filename to load the dictionary from
 **/
BOOLEAN load_data(struct checker_system* checker, const char * filename)
{
    char * input = (char*)malloc (MAX_INPUT);
    FILE * fp;
    struct word * newWord;

    checker->word_file = filename;

    if (!(fp = fopen (filename, "r")))
    {
        printf("Filed to open file\n");
        return EXIT_FAILURE;
    }
 
    while (fgets (input, MAX_INPUT, fp) != NULL)
    {
        if (input[strlen(input)-1] == '\n')
        {
            input[strlen(input)-1] = '\0';
        }

        newWord = malloc (sizeof (struct word));
        newWord = make_word(input);
      
        list_add(checker->list, newWord);
    }
  
    fclose (fp);
  
    printf ("\nLoaded dictionary with %d words.\n",
            (int)checker->list->num_words);
  
	  return TRUE;
}

/**
 * @param checker the datastructure used to manage the system
 **/
void system_free(struct checker_system* checker)
{
    list_free (checker->list);
    
    /*
    printf ("List Free Successfull\n");
    printf ("Checker Free Successfull\n");
    */
}

BOOLEAN get_input (char * input, BOOLEAN lower)
{

    int i = 0;
    BOOLEAN result = FALSE;
  
    fgets (input, MAX_INPUT, stdin);
    
    if (input == NULL || strlen(input) == 1)
    {
        return FALSE;
    }
    else
    {
        result = TRUE;
    }
  
    /* Remove new line */
    if (input [strlen(input)-1] == '\n')
    {
      input [strlen(input)-1] = '\0';
    }
  
    if (lower)
    {
        /* convert input to lower case */
        while (input[i])
        {
            input[i] = tolower(input[i]);
            i++;
        }
    }

    return result;
  
}

/* function to check for input of new line or ctrl+D */
BOOLEAN check_input (char * input)
{
    if (!strlen(input) || input[0] == '\n')
    {
        return FALSE;
    }
    else
    {
        return TRUE;
    }
}

