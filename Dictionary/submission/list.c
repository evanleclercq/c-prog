/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2015 Assignment #3
* Full Name        : Evan le Clercq
* Student Number   : S3516635
* Start up code provided by Paul Miller
***********************************************************************/
#include "list.h"

/**
 * @file list.c contains implementation of functions for managing the word lists
 * used by the program.
 **/

#ifdef WITH_LEVENSHTEIN
/**
 * @param first the first string to compare
 * @param first_len the number of characters in the first string to compare
 * @param second the second string to compare
 * @param second_len the number of characters in the second string to compare
 * @return the minimum number of edits required to turn the first string into
 * the second string 
 **/

int levenshtein(const char * first, int first_len, 
                const char * second, int second_len)
{
    int shorten_both, shorten_first, shorten_second;

    /* if either string is shorter than the threshold, difference is 
       inserting all chars from the other
     */
    if (first_len < THRESHOLD) return second_len;
    if (second_len < THRESHOLD) return first_len;
    printf("%s:%s\n", first, second);
    /* if the difference in the length of the strings is greater than the 
     * threshold, they are not similar
     */
    if(abs(strlen(first) - strlen(second)) > THRESHOLD)
    {
            return INT_MAX;
    }

    /* if last letters are the same, the difference is whatever is
     * required to edit the rest of the strings
     */
    if (first[first_len] == second[first_len])
            return levenshtein(first, first_len - 1, second, 
                               second_len - 1);

    /* else try:
     *      changing last letter of first to that of second; or
     *      remove last letter of first; or
     *      remove last letter of second,
     * any of which is 1 edit plus editing the rest of the strings
     */
    shorten_both = levenshtein(first, first_len - 1, second, 
                               second_len - 1);
    shorten_first = levenshtein(first, first_len,     second, 
                                second_len - 1);
    shorten_second = levenshtein(first, first_len - 1, second, 
                                 second_len    );
    return (shorten_both <= shorten_first) ? 
            ((shorten_both <=  shorten_second) ? 
             shorten_both + 1 : shorten_second + 1)
            : shorten_first + 1;
}
#endif/* WITH_LEVENSHTEIN */

BOOLEAN list_init (struct word_list * list)
{
  
    memset (list, 0, sizeof(struct word_list));  
    return TRUE;
  
}


BOOLEAN list_add (struct word_list * list, struct word * word)
{
  
    struct list_node * current, * prev = NULL;
    struct list_node * new;
    assert (list);
  
    new = malloc (sizeof (struct list_node));
    new->next = NULL;
    new->data = word;
  
    /* if list is empty */
    if (list->head == NULL)
    {
      list->head = new;
      ++list->num_words;
      return TRUE;
    }
  
    /* iterate over list */
    current = list->head; 
    while (current != NULL && data_cmp (current->data, word) < 0)
    {
        prev = current;
        current = current->next;
    }
  
    
    /* check if duplicate entry */
    if (current != NULL && !(data_cmp(current->data, word)))
    {
       /* printf ("Error: '%s' is already in the dictionary."
                " We do not accept duplicate.\n", word->word);*/
        return FALSE;
    }
    
  
    /* insertion at head */
    if (prev == NULL)
    {
        new->next = list->head;
        list->head = new;
    }
    else if (current == NULL) /*insertion at end*/
    {
        prev->next = new;
        current = new;
    }
    else /* insertion mid way through */
    {
        prev->next = new;
        new->next = current;
    }
  
    /* increment total number of words */
    ++list->num_words;  
  
    return TRUE;
  
}


BOOLEAN list_del (struct word_list * list, struct word * word)
{
  
    struct list_node * current, * prev = NULL;
  
    current = list->head;
  
    while (current != NULL)
    {
      
        if (!(data_cmp (current->data, word)))
        {
            if (prev == NULL)
            {
                list->head = current->next;
                free(current->data);
                free(current);
            }
            else
            {
                prev->next = current->next;
                free (current->data);
                free (current);
            }
            --list->num_words;
            return TRUE;
        }
      
        prev = current;
        current = current->next;
      
    }
  
    return FALSE;
  
}


void list_free (struct word_list * list)
{
  
    struct list_node * current = list->head;
  
    while (current != NULL)
    {
        struct list_node * prev;
        prev = current;
        current = current->next;
        free (prev->data);
        free (prev);
    }
  
}

int data_cmp (struct word * word1, struct word * word2)
{
    return strcmp (word1->word, word2->word);
}



struct word * make_word (char * word)
{
  
    struct word * new;
    new = malloc (sizeof(struct word));
  
    new->word = (char*)malloc (MAX_INPUT);
  
    strcpy (new->word, word);
    new->count = 0;
  
    return new;
  
}
