/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2015 Assignment #3
* Full Name        : Evan le Clercq
* Student Number   : S3516635
* Start up code provided by Paul Miller
***********************************************************************/
#ifndef SHARED_H
#define SHARED_H

#define MAX_INPUT 20
#define LINE_LENGTH 100
#define DELIMIT ")(,.-;:'! \t\r\n\v\f"

/**
 * @file shared.h defines the data structures that need to be shared across all
 * files. This is mainly here so we can avoid recursive includes for commonly 
 * used structures
 **/
/**
 * definition of the BOOLEAN datatype
 **/
typedef enum
{
        /**
         * value for FALSE (0)
         **/
        FALSE,
        /** 
         * value for TRUE (1)
         **/
        TRUE
} BOOLEAN;
#endif
